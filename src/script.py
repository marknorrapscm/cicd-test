import datetime
from globals import Settings
from gitlab_api import GitlabAPI
from requests_html import HTMLSession


def writeLineToFile(file, line):
    timestamp = str(datetime.datetime.now())
    file.write(f'{timestamp}: {line}\n')


# Read data from gov site (test)
headers = {'User-Agent': Settings.USER_AGENT}

session = HTMLSession()
req = session.get('https://coronavirus.data.gov.uk', headers=headers)
req.html.render(sleep=3)

xpathTotalCases = '/html/body/div/div/div[6]/div/div[1]/table/tbody/tr[2]/td[2]'
xpathTotalDeaths = '/html/body/div/div/div[6]/div/div[1]/table/tbody/tr[2]/td[3]'

totalNoOfCases = req.html.xpath(xpathTotalCases)[0].text
totalNoOfDeaths = req.html.xpath(xpathTotalDeaths)[0].text

outputStr = f'\n# of cases: {totalNoOfCases}\n# of deaths: {totalNoOfDeaths}\n'
print(outputStr)


# Write data to file
file = open(Settings.ARTIFACT_FILE_PATH, "a+")

jobID = GitlabAPI.getIdOfMostRecentJob()
artifact = GitlabAPI.readArtifactFromJob(jobID)

file.write(artifact)
writeLineToFile(file, outputStr)

file.close()
