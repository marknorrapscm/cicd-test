import requests
import json
from globals import Settings


class GitlabAPI:
    @staticmethod
    def getIdOfMostRecentJob():
        url = f'https://gitlab.com/api/v4/projects/{Settings.GITLAB_PROJECT_ID}/jobs?scope[]=success'
        headers = {
            'User-Agent': Settings.USER_AGENT,
            'PRIVATE-TOKEN': Settings.GITLAB_API_TOKEN
        }

        response = requests.get(url, headers=headers)
        responseParsed = response.json()
        jobID = responseParsed[0]['id']
        print(f'Job #{jobID} was read from getIdOfMostRecentJob()')
        return jobID

    @staticmethod
    def readArtifactFromJob(jobID):
        url = f'https://gitlab.com/api/v4/projects/{Settings.GITLAB_PROJECT_ID}/jobs/{jobID}/artifacts/test123.txt'
        headers = {'User-Agent': Settings.USER_AGENT}

        response = requests.get(url, headers=headers)
        return response.text
